#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from decouple import config
from .settings import BASE_DIR

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# CORS
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = (
    u"http://localhost:8080",
    u"http://127.0.0.1:8080"
)
