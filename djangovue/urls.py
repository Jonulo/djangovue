#!/usr/bin/env python
# -*- coding: utf-8

""" vue-django URL Configuration """

from django.contrib import admin
from django.conf.urls import url
from django.conf import settings
from django.views.static import serve
from django.urls import path, include
from rest_framework.documentation import include_docs_urls

# from rest_framework_jwt.views import verify_jwt_token
# from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

urlpatterns = [
    path(
        'admin/',
        admin.site.urls
    ),

    # JWT auth
    # path('api/auth/obtain_token/', obtain_jwt_token),
    # path('api/auth/refresh_token/', refresh_jwt_token),
    # path('api/auth/token-verify/', verify_jwt_token),

    # Documentation
    path(
        'api/documentation/',
        include_docs_urls(title="API Documantation")
    ),
    # Project paths
    path(
        'api/v1/',
        include(
            ('api.v1.sinister.urls', 'sinister'),
            namespace='sinister'
        ),
    ),
    # Static and media files
    url(
        r'^static/(?P<path>.*)$', serve,
        {'document_root': settings.STATICFILES_DIRS}),
    url(r'^media/(?P<path>.*)$', serve,
        {'document_root': settings.MEDIA_ROOT}),
]

if settings.ENV != 'dev':
    # Load VueApp files
    urlpatterns.append(path('', include('frontend.urls')))
