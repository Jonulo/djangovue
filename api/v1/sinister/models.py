#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Utils
import os
import random
import datetime

# Django
from django.db import models
from django.conf import settings

VERSION = (0, 0, 1)
__author__ = "Andrés Aguilar"
__email__ = "andresyoshimar@gmail.com"
__date__ = "14/10/2019"
__modified__ = "14/10/2019"
__version__ = ".".join([str(x) for x in VERSION])


def upload_reformat_name(instance, filename, related_model=None):
    """
    Format the name of the file consisting of the date,
    random number and extension
    The directory name: app/model/pk/directory/
    """
    name, ext = os.path.splitext(filename)
    name = '%s_%s' % (
        datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        '{:05d}'.format(random.randint(0, 99999))
    )
    directory = '%s/%s/' % (
        instance._meta.app_label,
        str(instance.__class__.__name__).lower()
    )

    if related_model:
        if related_model.pk:
            directory = '%s%s/' % (directory, str(related_model.id))

    return '%s%s%s' % (directory, name, ext.lower())


def upload_sinister_file_path(instance, filename):
    related_model = instance
    return upload_reformat_name(instance, filename, related_model)


class Highway(models.Model):
    """ Highway model """
    name = models.CharField(
        verbose_name="Nombre", blank=False, null=False, max_length=500
    )
    codename = models.CharField(
        verbose_name="Nombre clave", blank=False, null=False, max_length=15
    )
    desctiption = models.TextField(
        verbose_name="Descripción", blank=True, null=True
    )

    def __str__(self):
        return self.name


class Provider(models.Model):
    """ Provider model """
    name = models.CharField(
        verbose_name="Nombre", max_length=150, blank=False, null=False
    )
    last_name = models.CharField(
        verbose_name="Appelido paterno", max_length=100, blank=False,
        null=False
    )
    mothers_last_name = models.CharField(
        verbose_name="Apellido materno", max_length=100, blank=True, null=True
    )

    def __str__(self):
        return "{} {}".format(self.name, self.last_name)


class Sinister(models.Model):
    """ Sinister model """
    number = models.CharField(
        verbose_name="Número de siniestro", max_length=200, blank=False,
        null=False, unique=True
    )
    date = models.DateField(
        verbose_name="Fecha de ocurrido", blank=False, null=False
    )
    highway = models.ForeignKey(
        Highway, verbose_name="Autopista", blank=False, null=False,
        on_delete=models.CASCADE
    )
    body = models.CharField(
        verbose_name="Cuerpo", max_length=2, blank=False, null=False
    )
    police = models.CharField(
        verbose_name="Póliza", max_length=100, blank=False, null=False
    )
    kilometer = models.IntegerField(
        verbose_name="Kilómetro", blank=False, null=False
    )
    meter = models.IntegerField(
        verbose_name="Metro", blank=False, null=False
    )
    provider = models.ForeignKey(
        Provider, verbose_name="Proveedor", on_delete=models.CASCADE,
        null=True, blank=True
    )
    budget = models.FileField(
        verbose_name="Presupuesto", upload_to=upload_sinister_file_path,
        max_length=1000, blank=True, null=True
    )

    def __str__(self):
        return self.number
