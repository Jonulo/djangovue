#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django REST Framework
from rest_framework.serializers import ModelSerializer

# Models
from .models import Provider, Highway, Sinister

VERSION = (0, 0, 1)
__author__ = "Andrés Aguilar"
__email__ = "andresyoshimar@gmail.com"
__date__ = "14/10/2019"
__modified__ = "14/10/2019"
__version__ = ".".join([str(x) for x in VERSION])


class ProviserSerializer(ModelSerializer):
    """ Generic Provider serializer """
    class Meta:
        model = Provider
        fields = '__all__'


class HighwaySerializer(ModelSerializer):
    """ Generic Highway serializer """
    class Meta:
        model = Highway
        fields = '__all__'


class SinisterSerializer(ModelSerializer):
    """ Generic Sinister serializer """
    class Meta:
        model = Sinister
        fields = '__all__'
