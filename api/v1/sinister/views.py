#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django REST Framework
from rest_framework.viewsets import ModelViewSet

# Serializers
from .serializers import (
    ProviserSerializer, HighwaySerializer, SinisterSerializer
)

# Models
from .models import Provider, Highway, Sinister

VERSION = (0, 0, 1)
__author__ = "Andrés Aguilar"
__email__ = "andresyoshimar@gmail.com"
__date__ = "14/10/2019"
__modified__ = "14/10/2019"
__version__ = ".".join([str(x) for x in VERSION])


class ProviderView(ModelViewSet):
    """ Provider ViewSet """
    queryset = Provider.objects.all()
    serializer_class = ProviserSerializer


class HighwayView(ModelViewSet):
    """ Highway ViewSet """
    queryset = Highway.objects.all()
    serializer_class = HighwaySerializer


class SinisterView(ModelViewSet):
    """ Sinister ViewSet """
    queryset = Sinister.objects.all()
    serializer_class = SinisterSerializer
