#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Django
from django.urls import path, include

# REST Framework
from rest_framework.routers import DefaultRouter

# Models
from .views import ProviderView, HighwayView, SinisterView

VERSION = (0, 0, 1)
__author__ = "Andrés Aguilar"
__email__ = "andresyoshimar@gmail.com"
__date__ = "14/10/2019"
__modified__ = "14/10/2019"
__version__ = ".".join([str(x) for x in VERSION])

# Router definition
router = DefaultRouter()

# Register views
router.register(r'providers', ProviderView)
router.register(r'highways', HighwayView)
router.register(r'sinisters', SinisterView)

urlpatterns = [
    path(
        '',
        include(
            (router.urls, 'sinisters')
        )
    ),
]
