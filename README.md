# Vue & Django

Simple project configuration for Django + Vue.js

## Installation

Clone Project

1. Create Virtual env
```bash
python3 -m venv .venv
```

2. Install dependencies
```bash
# For development
pip install -r requirements/local.txt
# For production environment
pip install -r requirements/production.txt
```

3. Setup your database

In this example a Postgres Database is already setup for production environment only.

For developmen environment uses SQLite3

4. Create a ```.env``` file in the root directory. This will handle your config variables.
Example:
```
ENVIRONMENT=dev
SECRET_KEY=N0t_A_S3Cr3T
DEBUG=True
DATABASE_NAME=db_name
DATABASE_USER=db_user
DATABASE_PASSWORD=db_password
LANGUAGE_CODE=es-mx
TIME_ZONE=America/Mexico_City
DEFAULT_EMAIL_FROM=sender@mail.com
SERVER_EMAIL=server.com
EMAIL_HOST=
EMAIL_PORT=465
EMAIL_HOST_USER=email_user
EMAIL_HOST_PASSWORD=email_passwd
EMAIL_USE_TLS=True
```

For the `ENVIRONMENT` variable, values can be: `dev` or `production`

5. Run migrations
```bash
python manage.py migrate
```

6. Create Superuser
```bash
python manage.py createsuperuser
```

7. Setup Frontend
```bash
cd frontend/vueapp && npm install
```

You're done

## Build for Production

1. Build Frontend
(You may have to go to your frontend dir first again by using ```cd frontend/vueapp```)

```bash
npm run build
```

2. Collect statics
(You may have to go back to your root dir first)
```bash
python manage.py collectstatic --no-confirm
```

3. Change `ENVIRONMENT`value to `production`

4. Test
Start the server
```bash
python manage.py runserver
```
This time go to ```localhost:8000``` and check if Django serves the frontend correctly
