import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'
import pruebas from './components/dashboard.vue'
import loadProviders from './components/loadProviders.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/pruebas',
      name: 'tabla',
      component: pruebas
    },
    {
      path: '/providers',
      name: 'loadProviders',
      component: loadProviders
    }
  ]
})
